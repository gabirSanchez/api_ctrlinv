<?php

require APPPATH . 'libraries/REST_Controller.php';
 
class Validate extends REST_Controller {
    protected $ACCESS = false;
    protected $X_API_KEY = '16fe3fb7030eb8dbf7e1f825b55b9bb7';
    protected $HEADERS;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function validate_public() {

        $this->HEADERS = $this->input->request_headers();

        if ( !isset($this->HEADERS['x-api-key']) || $this->HEADERS['x-api-key'] !== $this->X_API_KEY ) {
            $this->response(array(
                'status' => "error",
                'data' => -1,
                'message' => "Punto de acceso no permitido"
            ), REST_Controller::HTTP_OK);
        } else {
            $this->ACCESS = true;
        }
       
    }

    public function validate() {

        $this->HEADERS = $this->input->request_headers();

        if ( (!isset($this->HEADERS['x-api-key']) || $this->HEADERS['x-api-key'] !== $this->X_API_KEY ) || ( !isset($this->HEADERS['Authorization']) && empty($this->HEADERS['Authorization']) ) ) {
            $this->response(array(
                'status' => "error",
                'data' => -1,
                'message' => "Punto de acceso no permitido"
            ), REST_Controller::HTTP_OK);
        } else {
            $this->ACCESS = true;
        }
       
    }

    public function JWT_decode () {
        $sinbarear = explode(" ", $this->HEADERS['Authorization']);
        $decode = $this->jwt->decode( $sinbarear[1] , $this->X_API_KEY );
        return $decode;
    }

    public function JWT_encode( $data ) {
        $encode = $this->jwt->encode( $data , $this->X_API_KEY );
        return $encode;
    }
}