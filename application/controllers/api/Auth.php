<?php
require 'Validate.php';
 
class Auth extends Validate {
  
  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
      
       parent::__construct();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        parent::validate();
        
        if ( $this->ACCESS ) {
            if(!empty($id)){
                $data = $this->db->get_where("usuarios", ['id' => $id])->row_array();
            }else{
                $data = $this->db->get("usuarios")->result();
            }     
            $this->response($data, REST_Controller::HTTP_OK);
        }
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        parent::validate();
        
        if ( $this->ACCESS ) {
            $input = $this->input->post();
            $this->db->insert('usuarios',$input);    
            $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
        }
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        parent::validate();
        
        if ( $this->ACCESS ) {
            $input = $this->put();
            $this->db->update('usuarios', $input, array('id'=>$id));    
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        }
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        parent::validate();
        
        if ( $this->ACCESS ) {
            $this->db->delete('usuarios', array('id'=>$id));
            $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
        }
    }

    /*
    * POST para logear el usuario
    */
    public function login_post() 
    {
    
        parent::validate_public();
        
        if ( $this->ACCESS ) {
            $input = json_decode($this->input->raw_input_stream );
            $data = $this->db->get_where(
                "usuarios", 
                [
                    'usuario' => $input->usuario,
                    'contrasenia' => MD5($input->contrasenia)
                ]
            )->row_array();   
            
            if ( gettype($data) == 'NULL' ) { 
                $resp = array(
                    'status' => "error",
                    'data' => -1,
                    'message' => "El usuario no se encontro"
                );
            } else {
                $resp = array(
                    'status' => "success",
                    'data' => parent::JWT_encode( $data )
                );
            }
            $this->response( $resp , REST_Controller::HTTP_OK);
        }
    }


}