<?php
require 'Validate.php';
 
class Vehiculo extends Validate {
   /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        parent::validate();
        
        if ( $this->ACCESS ) {
            $this->db->select("vehiculos.id as id, tipo, num_llantas, potencia, nombres, apellidos");
            if(!empty($id)){
                $this->db->join('usuarios', 'vehiculos.usuario_id=usuarios.id', 'left');
                $data = $this->db->get_where("vehiculos", ['vehiculos.id' => $id])->row_array();
            }else{
                $this->db->join('usuarios', 'vehiculos.usuario_id=usuarios.id', 'left');
                $data = $this->db->get("vehiculos")->result();
            }
            
            if ( gettype($data) == 'NULL' ) { 
                $resp = array(
                    'status' => "error",
                    'data' => -1,
                    'message' => "No se encontro vehiculos"
                );
            } else {
                $resp = array(
                    'status' => "success",
                    'data' => $data,
                    'total' => count($data)
                );
            }
           
            $this->response( $resp , REST_Controller::HTTP_OK);
        }
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        parent::validate();
        
        if ( $this->ACCESS ) {
            $input = json_decode($this->input->raw_input_stream );
            $user_session = parent::JWT_decode();
            $input->usuario_id = $user_session->id;
            $data = $this->db->insert('vehiculos',$input);   
            
            $resp = array(
                'status' => "success",
                'data' => $data,
            );
            $this->response($resp , REST_Controller::HTTP_OK);
        }
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        parent::validate();
        
        if ( $this->ACCESS ) {
            $input = $this->put();
            $this->db->update('vehiculos', $input, array('id'=>$id));    
            $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
        }
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        parent::validate();
        
        if ( $this->ACCESS ) {
            $this->db->delete('vehiculos', array('id'=>$id));
            $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
            
        }
    }
}