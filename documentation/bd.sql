CREATE TABLE `ctrlinv`.`usuarios` (
    `id` INT(11) NOT NULL AUTO_INCREMENT , 
    `nombres` VARCHAR(200) NOT NULL , 
    `apellidos` VARCHAR(200) NOT NULL , 
    `usuario` VARCHAR(50) NOT NULL ,
    `correo` VARCHAR(200) NOT NULL , 
    `contrasenia` VARCHAR(50) NOT NULL , 
    PRIMARY KEY (`id`)) 
    ENGINE = InnoDB; 

CREATE TABLE `ctrlinv`.`vehiculos` (
    `id` INT(11) NOT NULL AUTO_INCREMENT , 
    `tipo` VARCHAR(100) NOT NULL , 
    `num_llantas` INT(11) NOT NULL , 
    `potencia` VARCHAR(200) NOT NULL ,
    `usuario_id` INT(11) NOT NULL DEFAULT 1, 
    PRIMARY KEY (`id`)) 
    ENGINE = InnoDB